#!/bin/bash

dir="$(dirname "$(readlink -f "$0")")"

. "$dir/../util.sh"

build_package() { # $1 - package name, $2 - package description, $3 - edition suffix
	export compass_name="$1"
	export compass_ver="$pkgver"
	export compass_rel="$pkgrel"
	export compass_desc="The official GUI for MongoDB$2 - binary version"
	export compass_edition="$3"

	local package_build_dir="$(prepare_build_directory "$build_dir" "mongodb-$1-bin")"
	cp "$dir/../_common/"* "$package_build_dir/"
	cp "$dir/template/"* "$package_build_dir/"
	envsubst '$compass_name $compass_ver $compass_rel $compass_desc $compass_edition' < "$dir/template/PKGBUILD" > "$package_build_dir/PKGBUILD"
}

build_dir="${1:-}"

if [ -n "$build_dir" ] && ! [ -d "$build_dir/" ]; then abort 2 'The provided build directory does not exist, aborting...'; fi

for envfile in "$dir/"*'.env'; do
	case "$(basename "$envfile")" in
		'mongodb-compass-bin.env')
			. "$envfile"
			editions=(
				'compass-community; - Community Edition; Community'
				'compass-readonly; - Readonly Edition; Readonly'
				'compass-isolated; - Isolated Edition; Isolated Edition'
			)
		;;
		'mongodb-compass-beta-bin.env')
			. "$envfile"
			editions=(
				'compass-beta; - beta version; Beta'
				'compass-community-beta; - Community Edition - beta version; Community Beta'
				'compass-readonly-beta; - Readonly Edition - beta version; Readonly Beta'
				'compass-isolated-beta; - Isolated Edition - beta version; Isolated Edition Beta'
			)
		;;
		*)
			editions=()
		;;
	esac

	for edition in "${editions[@]}"; do
		IFS=';' read -a fields <<< "$edition"
		echo "mongodb-${fields[0]}-bin"
		if [ -n "$build_dir" ]; then build_package "${fields[0]}" "${fields[1]}" "${fields[2]}"; fi
	done
done
