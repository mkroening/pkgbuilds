#!/bin/false

# Save the initial state of shopts and enable them
declare -A shopts
shopts=(['dotglob']='' ['extglob']='' ['nullglob']='')
for shopt_name in "${!shopts[@]}"; do
	shopts["$shopt_name"]="$(shopt -q "$shopt_name" && printf 'true' || printf 'false')"
	shopt -s "$shopt_name"
done

# Reset shopts to their initial state on exit
restore_shopts() {
	for shopt_name in "${!shopts[@]}"; do
		"${shopts[$shopt_name]}" || shopt -u "$shopt_name"
	done
}
trap restore_shopts EXIT

abort() {
	echo "$2"
	exit "$1"
}

prepare_build_directory() { # $1 - build directory, $2 - package name
	local build_dir="$(echo "$1" | sed -E 's|(.*)/?|\1|')" # normalize trailing slash
	local package_build_dir="$build_dir/$2"
	mkdir "$package_build_dir/"
	git clone --quiet "ssh://aur@aur.archlinux.org/$2.git" "$package_build_dir/"
	rm -rf "$package_build_dir/"!(.git|.|..)
	echo "$package_build_dir"
}
