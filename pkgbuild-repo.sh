#!/bin/bash
# dependencies: git pacman

dir="$(dirname "$(readlink -f "$0")")"

[ -f "$dir/util.sh" ] && . "$dir/util.sh" || (echo "util.sh not found"; exit 2)

if [ "${1:-}" != 'dev' ] && [ "${1:-}" != 'prod' ]; then
	abort 1 'USAGE
  pkgbuild-repo COMMAND [-a]
COMMANDS
  dev           builds packages changed since the last commit and updates
                package list in README
  prod          builds packages changed in the last commit and pushes packages that
                changed to the AUR
ARGUMENTS
  -a            builds all packages regardless of if they changed or not'
fi

cd "$dir/"

tmp_dir="$dir/_tmp"
rm -rf "$tmp_dir/"
mkdir "$tmp_dir/"

build_dir="$tmp_dir/build"
mkdir "$build_dir/"

source_dir="$tmp_dir/source"
mkdir "$source_dir/"

package_list=''
changed_packages=''

build_package() {
	local folder_path="$(echo "$1" | sed -E 's|(.*)/?|\1|')" # normalize trailing slash
	local folder_name="$(basename "$folder_path")"

	if [ -f "$folder_path/bundle.sh" ]; then
		while read line; do
			echo "Building $line using $folder_name/bundle.sh"
			package_list="$package_list$line"$'\n'
		done <<< "$(bash "$folder_path/bundle.sh" "$build_dir")"
	else
		echo "Building $folder_name"
		package_list="$package_list$folder_name"$'\n'
		cp "$dir/_common/"* "$folder_path/"* "$(prepare_build_directory "$build_dir" "$folder_name")/"
	fi
}

[ "${2:-}" = '-a' ] && build_all=1 || build_all=0

if [ "$build_all" = '0' ]; then
	if [ "$1" = 'dev' ]; then
		print=1
		while read line; do
			if [[ "$line" =~ ^The\ following\ paths\ are\ ignored\ by\ one\ of\ your\ \.gitignore\ files.*$ ]]; then
				print=0
			elif [ "$line" = 'Use -f if you really want to add them.' ]; then
				print=1
			elif [ "$print" = '1' ]; then
				echo "$line"
			fi
		done <<< "$(git add --intent-to-add -- * 2>&1)"
		changes="$(git diff HEAD --dirstat=files,0)"
	elif [ "$1" = 'prod' ]; then
		changes="$(git diff HEAD~1 HEAD --dirstat=files,0)"
	fi

	changes="$(echo "$changes" | awk '{split($2,a,"/"); print a[1]}' | sort | uniq)"

	if echo "$changes" | grep -q '^_common'; then
		build_all=1
	else
		while read folder; do
			if [ -n "$folder" ] && [ -d "$dir/$folder/" ]; then
				build_package "$dir/$folder"
			fi
		done <<< "$changes"
	fi
fi

if [ "$build_all" = '1' ]; then
	for folder in "$dir/"!(_common)'/'; do
		if ! git check-ignore -q "$folder"; then
			build_package "$folder"
		fi
	done
fi

for package in "$build_dir/"*'/'; do
	cd "$package/"
	package_name="$(basename "$package")"

	echo "Downloading sources for $package_name"

	printf '# Maintainer: %s <%s>\n%s' "$(git config --get user.name)" "$(git config --get user.email)" "$(cat "${package}PKGBUILD")" > "${package}PKGBUILD"
	package_source_dir="$source_dir/$package_name"
	mkdir "$package_source_dir/"
	export SRCDEST="$package_source_dir"
	updpkgsums "${package}PKGBUILD"
	makepkg --printsrcinfo | sed -z '$ s/\n\n$/\n/' > "${package}.SRCINFO"
	unset SRCDEST

	git add -A

	if [ "$(git cat-file -t HEAD 2> '/dev/null')" != 'commit' ] || ! git diff --quiet HEAD; then
		changed_packages="$changed_packages$package_name"$'\n'

		if [ "$1" = 'prod' ]; then
			git commit -m "$(git -C "$dir" log --pretty=%B -n 1)"
			git push
		fi
	fi
done

cd "$dir/"

if [ -n "$changed_packages" ]; then
	printf '\n\033[1m>>> The following packages have changed'
	if [ "$1" = 'prod' ]; then printf ' and have been pushed to the AUR'; fi
	printf ':\033[0m\n%s' "$changed_packages"

	if [ "$1" = 'dev' ]; then
		if [ "$build_all" = '0' ]; then
			package_list=''
			for folder in "$dir/"!(_common)'/'; do
				if ! git check-ignore -q "$folder"; then
					if [ -f "${folder}bundle.sh" ]; then package_list="$package_list$(bash "${folder}bundle.sh")"$'\n'
					else package_list="$package_list$(basename "$folder")"$'\n'; fi
				fi
			done
		fi
		package_list="$(printf "$package_list" | sort | sed -E 's|^(.*)$|* [\1](https://aur.archlinux.org/packages/\1/)|g')"
		export package_list
		envsubst '$package_list' < "$dir/README.template.md" > "$dir/README.md"
	fi
fi

